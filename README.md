#Notice

## 使用

```javascript
Notice.alert({title:'这儿是标题',content:'这儿是正文',ok_callback:function(){console.log('you clicked ok btn');},draggable:true});
Notice.confirm({title:'这儿是标题',content:'这儿是正文',ok_callback:function(){console.log('ok');},cancel_callback:function(){console.log('cancel');},draggable:true});
Notice.prompt({title:'这儿是标题',content:'这儿是正文',ok_callback:function(msg){console.log('msg:',msg)},cancel_callback:function(){console.log('cancel')},draggable:true});
Notice.msg({message:'这是一条提示信息', mode:'WARN', time:3000});
// Notice.msg 中 mode 有三种方式 FINE ERROR WARN , 分别代表 正常 , 错误 , 警告
```

## License

MIT License
